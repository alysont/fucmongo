// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    console.log('Unable to connect Mongodb server');
  }
  console.log('Connected to Mondodb server');

  // delete many
  // db.collection('Todos').deleteMany({
  //   text: 'Eat something',
  // })
  //   .then((result) => {
  //     console.log(result);
  //   });

  // delete one
  // db.collection('Todos').deleteOne({
  //   text: 'Eat something',
  // })
  //   .then((result) => {
  //     console.log(result);
  //   });

  // find one and delete
  // db.collection('Todos').findOneAndDelete({
  //   completed: false,
  // })
  //   .then((result) => {
  //     console.log(result);
  //   });

  db.collection('Users').deleteMany({
    name: 'Sashimi',
  })
    .then((result) => {
      console.log(result);
    });

  db.collection('Users').findOneAndDelete({
    location: 'Hong Kong',
  })
    .then((result) => {
      console.log(result);
    });


  // db.close();
});
