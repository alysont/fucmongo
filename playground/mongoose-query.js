const { ObjectID } = require('mongodb');
const { mongoose } = require('./../server/db/mongoose');
const { Todo } = require('./../server/models/todo');
const { User } = require('./../server/models/user');

const id = '_xxx';
const userId = 'xxx';

if (!ObjectID.isValid(userId)) {
  console.log('userId not valid');
}

// Todo.find({
//   _id: id,
// }).then((todos) => {
//   console.log(`Todos find: ${todos}`);
// });

// Todo.findOne({
//   _id: id,
// }).then((todo) => {
//   console.log(`Todo findOne: ${todo}`);
// });

// Todo.findById(id)
//   .then((todo) => {
//     if (!todo) {
//       console.log('id not found');
//     }
//     console.log(`Todo findById: ${todo}`);
//   })
//   .catch(e => console.log(e));

User.findById(userId)
  .then((user) => {
    if (!user) {
      return console.log('user not found');
    }
    console.log(`User findById: ${user}`);
  })
  .catch(e => console.log(e));
