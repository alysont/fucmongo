// const MongoClient = require('mongodb').MongoClient;
const { MongoClient, ObjectID } = require('mongodb');
const faker = require('faker');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    console.log('Unable to connect Mongodb server');
  }
  console.log('Connected to Mondodb server');

  db.collection('Todos').findOneAndUpdate({
    text: 'Eat something',
  }, {
    $set: {
      completed: true,
    },
  }, {
    returnOriginal: false,
  })
    .then((result) => {
      console.log(result);
    });

  db.collection('Users').findOneAndUpdate({
    name: 'Udon',
  }, {
    $inc: { age: 1 },
    $set: {
      location: faker.address.city(),
    },
  }, {
    returnOriginal: false,
  })
    .then((result) => {
      console.log(result);
    });
  // db.close();
});
